//
//  cPessoa.h
//  trabPDV
//
//  Created by Tiago Paiva on 21/03/15.
//  Copyright (c) 2015 Tiago. All rights reserved.
//

#ifndef __trabPDV__cPessoa__
#define __trabPDV__cPessoa__

#include <stdio.h>
#include <string>
#include <iostream>
#include "cData.h"
#include "cEndereco.h"

class cPessoa{

public:
    cPessoa();
    ~cPessoa();
    std::string getName(void);
    std::string getBirth(void);
    std::string getCivilState(void);
    char getSex(void);
    std::string getAddress(void);
    
    void enterNameAndBirthAndSexAndCivil(void);
    void setName(std::string name);
    void setBirth(std::string birth);
    
private:
    cData data;
    cEndereco endereco;
    
    std::string name;
    std::string birth;
    std::string civilState;
    char sex;
    std::string address;
    
};

#endif /* defined(__trabPDV__cPessoa__) */
