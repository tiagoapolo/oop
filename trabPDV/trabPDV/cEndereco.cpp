//
//  cEndereco.cpp
//  trabPDV
//
//  Created by Tiago Paiva on 21/03/15.
//  Copyright (c) 2015 Tiago. All rights reserved.
//

#include "cEndereco.h"

cEndereco::cEndereco()
{
    this->state = "";
    this->city = "";
    this->address = "";
    this->street = "";
    this->zip = "";
}

void cEndereco::setStreetAndCityAndStateAndZip(std::string Street, std::string City, std::string State, std::string Zip)
{
    this->street = Street;
    this->city = City;
    this->state = State;
    
    createAddress();
}

void cEndereco::createAddress()
{
    this->address = this->street + ", " + this->city + ", " + this->state + ", " + this->zip;
}

std::string cEndereco::getAddress(void)
{
    return this->address;
}

std::string cEndereco::enterStreetAndCityAndStateAndZip()
{
    printf("\nDigite a rua :");
    getline(std::cin, this->street);
    
    printf("\nDigite o CEP :");
    getline(std::cin, this->zip);
    
    printf("\nDigite a cidade :");
    getline(std::cin, this->city);
    
    printf("\nDigite o estado :");
    getline(std::cin, this->state);
    
    createAddress();
    
    return this->address;
}