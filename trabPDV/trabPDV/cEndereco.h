//
//  cEndereco.h
//  trabPDV
//
//  Created by Tiago Paiva on 21/03/15.
//  Copyright (c) 2015 Tiago. All rights reserved.
//

#ifndef __trabPDV__cEndereco__
#define __trabPDV__cEndereco__

#include <stdio.h>
#include <string>
#include <iostream>

class cEndereco{
    
public:
    cEndereco();
    void setStreetAndCityAndStateAndZip(std::string Street, std::string City, std::string State, std::string Zip);
    std::string enterStreetAndCityAndStateAndZip(void);
    std::string getAddress(void);
    
private:
    
    
    void createAddress(void);
    
    std::string street;
    std::string city;
    std::string state;
    std::string address;
    std::string zip;
};

#endif /* defined(__trabPDV__cEndereco__) */
