//
//  cData.cpp
//  trabPDV
//
//  Created by Tiago Paiva on 21/03/15.
//  Copyright (c) 2015 Tiago. All rights reserved.
//

#include "cData.h"



/////////////////////////////
/*
 *          getDate
 */
////////////////////////////
std::string cData::getDate(void)
{
    return this->date;
}

/////////////////////////////
/*
 *          setDayMonthYear
 */
////////////////////////////
void cData::setDayMonthYear(std::string day, std::string month, std::string year)
{
    this->day = day;
    this->month = month;
    this->year = year;
    
    createDate();
}

/////////////////////////////
/*
 *          enterDayMonthYear
 */
////////////////////////////
std::string cData::enterDayMonthYear(void)
{
    printf("\nDigite o dia :");
    getline(std::cin, this->day);
    printf("\nDigite o mes :");
    getline(std::cin, this->month);
    printf("\nDigite o ano :");
    getline(std::cin, this->year);
    
    createDate();
    
    return this->date;
}

/////////////////////////////
/*
 *          createDate
 */
////////////////////////////
void cData::createDate(void)
{
    if (atoi(this->day.c_str()) < 0 || atoi(this->day.c_str()) > 31)
    {
        printf("\nParâmetro \"Dia\" inválido");
        this->date = "";
        enterDayMonthYear();
    }
    if (atoi(this->month.c_str()) < 0 || atoi(this->month.c_str()) > 12)
    {
        printf("\nParâmetro \"Mes\" inválido");
        this->date = "";
        enterDayMonthYear();
    }
    if (atoi(this->year.c_str()) < 0 || atoi(this->year.c_str()) > 2100)
    {
        printf("\nParâmetro \"Ano\" inválido");
        this->date = "";
        enterDayMonthYear();
    }
    
    this->date = this->day + "/" + this->month + "/" + this->year;
}

/////////////////////////////
/*
 *          getDay
 */
////////////////////////////
std::string cData::getDay()
{
    return this->day;
}

/////////////////////////////
/*
 *          getMonth
 */
////////////////////////////
std::string cData::getMonth(void)
{
    return this->month;
}

/////////////////////////////
/*
 *          getYear
 */
////////////////////////////
std::string cData::getYear()
{
    return this->year;
}


