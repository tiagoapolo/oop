//
//  main.cpp
//  trabPDV
//
//  Created by Tiago Paiva on 21/03/15.
//  Copyright (c) 2015 Tiago. All rights reserved.
//

#include <iostream>

#include "cPessoa.h"
#include "cCompra.h"

int main(int argc, const char * argv[]) {

    char opt = 0;
    
    printf("\n------------- PDV Simples -------------");
    printf("\n|(V) Para nova venda\n|(S) Para sair");
    printf("\n|\n|Opção:");
    scanf("%c", &opt); getchar();
    while (opt == 'v' || opt == 'V') {
        cPessoa cliente;
        cCompra caixaVenda;
        
        cliente.enterNameAndBirthAndSexAndCivil();
        caixaVenda.sellItemAndPriceAndQnt();
        
        caixaVenda.printClientData(cliente);
        caixaVenda.payItem();
        
        
        //caixaVenda.~cCompra();
        //cliente.~cPessoa();
        
        printf("\nDeseja fazer uma nova venda ? (V/N)");
        getchar();
        scanf(" %c", &opt);
        getchar();
        
        printf("\n\n");
    }
    printf("\n---------------------------------------\n");

    
    return 0;
}
