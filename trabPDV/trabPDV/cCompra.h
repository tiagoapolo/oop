//
//  cCompra.h
//  trabPDV
//
//  Created by Tiago Paiva on 21/03/15.
//  Copyright (c) 2015 Tiago. All rights reserved.
//

#ifndef __trabPDV__cCompra__
#define __trabPDV__cCompra__

#include <stdio.h>
#include <string>
#include <iostream>

#include "cPessoa.h"

class cCompra{
    
public:
    
    cCompra();
    ~cCompra();
    
    void sellItemAndPriceAndQnt(void);
    void payItem(void);
    void printClientData(cPessoa cliente);
    
private:
    
    std::string item;
    int qnt;
    float price;
    float pay;
};

#endif /* defined(__trabPDV__cCompra__) */
