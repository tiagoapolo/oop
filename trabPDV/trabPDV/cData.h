//
//  cData.h
//  trabPDV
//
//  Created by Tiago Paiva on 21/03/15.
//  Copyright (c) 2015 Tiago. All rights reserved.
//

#ifndef __trabPDV__cData__
#define __trabPDV__cData__

#include <stdio.h>
#include <string>
#include <iostream>



class cData{

public:
    void setDayMonthYear(std::string day, std::string month, std::string year);
    std::string enterDayMonthYear(void);
    std::string getDay(void);
    std::string getMonth(void);
    std::string getYear(void);
    std::string getDate(void);
    
private:
    std::string day;
    std::string month;
    std::string year;
    std::string date;
    void createDate(void);
};
#endif /* defined(__trabPDV__cData__) */

/*
*       Modelo de getter e setter
*   void set();
*   return get();
*   void set()
*   {
*   }
*   return get();
*
*
*
*/