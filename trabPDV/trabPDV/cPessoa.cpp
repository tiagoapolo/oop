//
//  cPessoa.cpp
//  trabPDV
//
//  Created by Tiago Paiva on 21/03/15.
//  Copyright (c) 2015 Tiago. All rights reserved.
//

#include "cPessoa.h"

////////////////////////////
/*
 *          Constructor
 */
////////////////////////////
cPessoa::cPessoa()
{
    this->birth = "";
    this->sex = 0;
    this->name = "";
    this->civilState = "";
}

cPessoa::~cPessoa()
{
    //endereco.~cEndereco();
    //data.~cData();
}

/////////////////////////////
/*
 *          getName
 */
////////////////////////////
std::string cPessoa::getName(void)
{
    return this->name;
}

/////////////////////////////
/*
 *          getAddress
 */
////////////////////////////
std::string cPessoa::getAddress()
{
    return this->address;
}

/////////////////////////////
/*
 *          setName
 */
////////////////////////////
void cPessoa::setName(std::string name)
{
    this->name = name;
}

/////////////////////////////
/*
 *          getBirth
 */
////////////////////////////
std::string cPessoa::getBirth()
{
    return this->birth;
}

/////////////////////////////
/*
 *          getCivilState
 */
////////////////////////////
std::string cPessoa::getCivilState()
{
    return this->civilState;
}

/////////////////////////////
/*
 *          getCivilState
 */
////////////////////////////
char cPessoa::getSex()
{
    return this->sex;
}

/////////////////////////////
/*
 *          setBirth
 */
////////////////////////////
void cPessoa::setBirth(std::string birth)
{
    this->birth = birth;
}

/////////////////////////////
/*
 *          enterNameAndBirthAndSexAndCivil
 */
////////////////////////////
void cPessoa::enterNameAndBirthAndSexAndCivil()
{
    printf("---------- Cadastro de pessoa ----------");
    printf("\nDigite o nome :");
    getline(std::cin, this->name);
    
    printf("\nEntre com a data de nascimento.");
    this->birth = data.enterDayMonthYear();
    
    printf("\nDigite o sexo(F/M) :");
    scanf("%c", &this->sex); getchar();
    
    printf("\nDigite o estado civil :");
    getline(std::cin, this->civilState);
    
    printf("\nEntre com o seu endereço.");
    this->address = endereco.enterStreetAndCityAndStateAndZip();
}


