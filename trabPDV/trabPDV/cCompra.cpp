//
//  cCompra.cpp
//  trabPDV
//
//  Created by Tiago Paiva on 21/03/15.
//  Copyright (c) 2015 Tiago. All rights reserved.
//

#include "cCompra.h"

cCompra::cCompra()
{
    
}

cCompra::~cCompra()
{
    
}


void cCompra::sellItemAndPriceAndQnt()
{
    printf("\n---------- Venda de Item ----------");
    printf("\nItem :");
    getline(std::cin, this->item);
    
    printf("\nPreço :");
    scanf("%f", &this->price);
    
    printf("\nQuantidade :");
    scanf("%d", &this->qnt);
    
    
}

void cCompra::payItem(void)
{
    float aux = 0.0;
    
    printf("\n---------- Pagamento ----------");
    printf("\nEntre com o pagamento :");
    scanf("%f", &this->pay);
    
    while (this->pay < this->price) {
        printf("\nPagamento inferior ao preço!");
        printf("\nFaltam: %.2f", this->price - pay);
        printf("\nInsira o montante faltante:");
        scanf("%f", &aux);
        
        if (aux < 0.00)
        {
            aux *= -1;
        }
        
        this->pay += aux;
    }

    if (this->pay > this->price)
    {
        printf("\nTroco: %.2f", this->pay - this->price);
    }
    
    printf("\n------- Pagamento efetuado -------");
}

void cCompra::printClientData(cPessoa cliente)
{
    printf("\n---------- Dados do Cliente ----------");
    std::cout << "\nCliente: " << cliente.getName();
    std::cout << "\nSexo: " << cliente.getSex();
    std::cout << "\nNascimento: " << cliente.getBirth();
    std::cout << "\nEstado civil: " << cliente.getCivilState();
    std::cout << "\nEndereço: " << cliente.getAddress();
}
